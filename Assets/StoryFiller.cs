﻿using System;

public class StoryFiller {

    //static CreateNode(string node ,string[]){
    //    return "dd";
    //};

    public static GamePlayManager.StoryNode FillStory()
    {
        //codigo de inicializacion
        GamePlayManager.StoryNode root = CreateNode("uno", new string[]{
            "dos","tres"
        });
        GamePlayManager.StoryNode nodo1 = CreateNode("cuatro", new string[]{
            "cinco","seis"
        });

        root.nextNode[0] = nodo1;
        GamePlayManager.StoryNode nodo2 = CreateNode("siete", new string[]{
            "ocho","nueve"
        });
        nodo1.nextNode[0] = nodo2;
        nodo2.nextNode[0] = nodo1;
        return root;
    }

        private static GamePlayManager.StoryNode CreateNode (string history, string[] options){
            GamePlayManager.StoryNode node = new GamePlayManager.StoryNode();

        node.history = history;
        node.answers = options;
        node.nextNode = new GamePlayManager.StoryNode[options.Length];
        return node;
        }

    }
